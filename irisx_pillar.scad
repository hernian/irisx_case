
THICKNESS = 3;
R_BODY = 10;
XS_BODY1 = 110;
YS_BODY1 = 60;
ZS_BODY1 = 50;

XS_BODY1_2 = XS_BODY1 * 2;
YS_BODY1_2 = YS_BODY1 + 2;
ZS_BODY1_2 = ZS_BODY1;

XS_BODY2 = 70;
YS_BODY2 = 40;
ZS_BODY2 = 25;

XS_BODY2_2 = XS_BODY2 + 20;
YS_BODY2_2 = YS_BODY2 * 2;
ZS_BODY2_2 = ZS_BODY2 * 2;


mirror([1, 0,0])
union(){
    difference(){
        box(XS_BODY1, YS_BODY1, ZS_BODY1, THICKNESS);
        translate([0, -1, 3]){
            rotate([0, -20, 0]){
                cube([XS_BODY1_2, YS_BODY1_2, ZS_BODY1_2], false);
            }
        }
    }
    translate([50, -YS_BODY2, 0]){
        difference(){
            box(XS_BODY2, YS_BODY2, ZS_BODY2, THICKNESS);
            translate([0, 0, 15]){
                rotate([8, 4, 0]){
                    translate([-10, 0, 0]){
                        cube([XS_BODY2_2, YS_BODY2_2, ZS_BODY2_2], false);
                    }
                }
            }
        }
    }
}


module box(xs, ys, zs, tk){
    union(){
        difference(){
            cube([xs, ys, zs], false);
            translate([tk, tk, 0]){
                cube([xs - tk * 2, ys - tk * 2, zs], false);
            }
        }
//        cube([xs, ys, tk], false);
    }
}

module roundedcube(xdim ,ydim ,zdim,rdim){
    hull(){
        translate([rdim,rdim,0])cylinder(h=zdim,r=rdim);
        translate([xdim-rdim,rdim,0])cylinder(h=zdim,r=rdim);

        translate([rdim,ydim-rdim,0])cylinder(h=zdim,r=rdim);
        translate([xdim-rdim,ydim-rdim,0])cylinder(h=zdim,r=rdim);
    }
}
